<?php
namespace SatSuite\CfdiStatus;

use Xaamin\XmlToArray\XmlParser;
use SatSuite\CfdiStatus\Status\CfdiStatus;
use SatSuite\CfdiStatus\Http\SimpleHttpClient;
use SatSuite\CfdiStatus\Contracts\HttpClientInterface;
use SatSuite\CfdiStatus\Exceptions\SatServiceUnavailableException;

class CfdiStatusQuery
{
    protected $production = true;
    protected $debug = false;

    protected $result = [];

    /**
     * Http client
     *
     * @var \SatSuite\CfdiStatus\Contracts\HttpClientInterface
     */
    protected $client = false;

    /**
     * Xml parser
     *
     * @var \Xaamin\XmlToArray\XmlParser
     */
    protected $parser;

    public function __construct(HttpClientInterface $client = null, XmlParser $parser = null)
    {
        $this->client = $client ? : new SimpleHttpClient();
        $this->parser = $parser ? : new XmlParser();
    }

    public function production($production = true)
    {
        $this->production = $production;

        return $this;
    }

    public function debugging($debugging = true)
    {
        $this->debug = $debugging;

        return $this;
    }

    public function make($expression)
    {
        $this->result = [];

        $url = $this->getUrlFoComprobanterStatusVerification($this->production);
        $body = $this->getComprobanteSoapEnvelop($expression);

        $headers = [
            'Content-type' => 'text/xml;charset="utf-8"',
            'Accept' => 'text/xml',
            'SOAPAction' => 'http://tempuri.org/IConsultaCFDIService/Consulta',
            'Connection' => 'close'
        ];

        $response = $this->client->call($url, $body, $headers);
        $status = $response['status'];
        $content =  $response['body'];

        $success = $status === 200;

        $data = [];

        if ($success) {
            $data = $this->parser->load($content)->parse()->get('Body.ConsultaResponse.ConsultaResult');

            if (isset($data['Estado']) && is_array($data['Estado'])) {
                $data['Estado'] = '';
            }
        } else {
            if ($status >= 500) {
                throw new SatServiceUnavailableException("Servicio del SAT de consulta de status no disponible ({$content})", $status);
            } else {
                throw new SatServiceUnavailableException("Error al solicitar el status de cancelación ({$content})", $status);
            }
        }

        $result = [
            'success' => $success,
            'data' => $data
        ];

        $this->result = $this->addDebug($result, $url, $response, $headers, $body);

        $status = new CfdiStatus($data);

        return $status;
    }

    public function getResult()
    {
        return $this->result;
    }

    protected function addDebug(array $result, $url, array $response, array $headers, $body)
    {
        if ($this->debug) {
            $result['debug'] = [
                'request' => [
                    'url' => $url,
                    'headers' => $headers,
                    'body' => $body
                ],
                'response' => [
                    'status' => $response['status'],
                    'body' => $response['body']
                ]
            ];
        }

        return $result;
    }

    protected function getUrlFoComprobanterStatusVerification($production)
    {
        return
            $production
                ? 'https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc'
                : 'https://pruebacfdiconsultaqr.cloudapp.net/ConsultaCFDIService.svc';
    }

    protected function getComprobanteSoapEnvelop($uri)
    {
        return
            '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" xmlns:c="http://tempuri.org/">'
                . '<s:Header/>'
                . '<s:Body>'
                . '<c:Consulta>'
                    . '<!--Optional:-->'
                    . "<c:expresionImpresa><![CDATA[{$uri}]]></c:expresionImpresa>"
                . '</c:Consulta>'
                . '</s:Body>'
            . '</s:Envelope>';
        ;
    }

}
