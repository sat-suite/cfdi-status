<?php
namespace SatSuite\CfdiStatus\Contracts;

interface ExpressionInterface
{
    /**
     * Gets the expression version to be generated
     *
     * @return string
     */
    public function getVersion();

    /**
     * Lodas xml from file
     *
     * @param string $path
     *
     * @return ExpressionInterface
     */
    public function load($path);

    /**
     * Sets the xml contents
     *
     * @param string $xml
     *
     * @return ExpressionInterface
     */
    public function with($xml);

    /**
     * Generate the expression from the given values if provided
     *
     * @param array $meta
     *
     * @return string
     */
    public function make(array $meta = []);

    /**
     * Returns the parsed array from the xml
     *
     * @return array
     */
    public function values();

    /**
     * Generates a url to sat with the expression to query status
     *
     * @param array $meta
     *
     * @return string
     */
    public function url(array $meta = []);
}