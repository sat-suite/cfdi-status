<?php
namespace SatSuite\CfdiStatus\Contracts;

interface HttpClientInterface
{
    public function call($url, $body, array $headers = []);
}