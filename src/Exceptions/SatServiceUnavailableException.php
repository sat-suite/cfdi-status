<?php
namespace SatSuite\CfdiStatus\Exceptions;

use Exception;

class SatServiceUnavailableException extends Exception
{
}