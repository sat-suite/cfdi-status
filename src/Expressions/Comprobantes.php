<?php
namespace SatSuite\CfdiStatus\Expressions;

use UnexpectedValueException;
use SatSuite\Support\ComprobanteVersionDiscovery;
use SatSuite\CfdiStatus\Contracts\ExpressionInterface;

class Comprobantes
{
    protected $discovery;
    protected $strategies = [];

    public function __construct(ComprobanteVersionDiscovery $discovery = null)
    {
        $this->discovery = $discovery ?: new ComprobanteVersionDiscovery();

        $this->defaults();
    }

    /**
     * Register the defaults expression builders
     *
     * @return Comprobantes
     */
    public function defaults()
    {
        $this->extend(new Comprobantes32());
        $this->extend(new Comprobantes33());
        $this->extend(new Comprobantes40());

        return $this;
    }

    /**
     * Loads a given xml file or a xml string
     *
     * @param string $xml
     *
     * @return ExpressionInterface
     */
    public function load($xml)
    {
        $version = $this->discovery->make($xml);

        return $this->using($version)->load($xml);
    }

    /**
     * Extend using new expression builder
     *
     * @param ExpressionInterface $strategy
     *
     * @return Comprobantes
     */
    public function extend(ExpressionInterface $strategy)
    {
        $this->strategies[$strategy->getVersion()] = $strategy;
    }

    /**
     * Get a given version strategy for expression builder
     *
     * @param string $version
     *
     * @return ExpressionInterface
     */
    public function using($version)
    {
        $strategy = isset($this->strategies[$version]) ? $this->strategies[$version] : null;

        if (!$strategy) {
            throw new UnexpectedValueException("There is no expression defined for version {$version}");
        }

        return $strategy;
    }

}