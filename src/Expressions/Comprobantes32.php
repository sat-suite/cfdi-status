<?php
namespace SatSuite\CfdiStatus\Expressions;

use UnexpectedValueException;

class Comprobantes32 extends Expression
{
    public function getVersion()
    {
        return '3.2';
    }

    public function make(array $meta = [])
    {
        $meta = $meta ? : $this->values();

        $emisor = htmlspecialchars(trim(isset($meta['rfc_emisor']) ? $meta['rfc_emisor'] : ''), ENT_XML1);
        $receptor = htmlspecialchars(trim(isset($meta['rfc_receptor']) ? $meta['rfc_receptor'] : ''), ENT_XML1);
        $uuid = isset($meta['uuid']) ? $meta['uuid'] : '';
        $total = $this->formatTotal(isset($meta['total']) ? $meta['total'] : 0);

        return "?re={$emisor}&rr={$receptor}&tt={$total}&id={$uuid}";
    }

    public function values()
    {
        if (!empty($this->_values)) {
            return $this->_values;
        }

        if ((string)$this->sxml->attributes()->version !== '3.2') {
            throw new UnexpectedValueException("Version is not 3.2");
        }

        $meta = [];

        $ns = $this->sxml->getNamespaces(true);

        if (empty($ns['cfdi'])) {
            throw new UnexpectedValueException("Cfdi namespace is not defined");
        }

        if (empty($ns['tfd'])) {
            throw new UnexpectedValueException("Tfd namespace is not defined");
        }

        $this->sxml->registerXPathNamespace('c', $ns['cfdi']);
        $this->sxml->registerXPathNamespace('t', $ns['tfd']);

        $emisor = current($this->sxml->xpath('//c:Comprobante//c:Emisor'));
        $receptor = current($this->sxml->xpath('//c:Comprobante//cfdi:Receptor'));
        $tfd = current($this->sxml->xpath('//t:TimbreFiscalDigital'));

        $meta = [
            'rfc_emisor' => (string)$emisor->attributes()->rfc,
            'rfc_receptor' => (string)$receptor->attributes()->rfc,
            'uuid' => (string)$tfd->attributes()->UUID,
            'total' => $this->formatTotal((string)$this->sxml->attributes()->total),
            'sello' => (string)$this->sxml->attributes()->sello
        ];

        unset($this->sxml);

        return $this->_values = $meta;
    }

    public function url(array $meta = [])
    {
        $expression = $this->make($meta);
        $meta = $meta ? : $this->values();

        $sello = isset($meta['sello']) ? substr($meta['sello'], -8) : '';

        return "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx{$expression}&fe={$sello}";

    }

    protected function formatTotal($value)
    {
        return str_pad(number_format(floatval($value), 6, '.', ''), 17, '0', STR_PAD_LEFT);
    }

}
