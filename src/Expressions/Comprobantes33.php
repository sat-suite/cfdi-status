<?php
namespace SatSuite\CfdiStatus\Expressions;

use UnexpectedValueException;

class Comprobantes33 extends Expression
{
    public function getVersion()
    {
        return '3.3';
    }

    public function make(array $meta = [])
    {
        $meta = $meta ? : $this->values();

        $emisor = htmlspecialchars(trim(isset($meta['rfc_emisor']) ? $meta['rfc_emisor'] : ''), ENT_XML1);
        $receptor = htmlspecialchars(trim(isset($meta['rfc_receptor']) ? $meta['rfc_receptor'] : ''), ENT_XML1);
        $uuid = isset($meta['uuid']) ? $meta['uuid'] : '';
        $total = $this->formatTotal(isset($meta['total']) ? $meta['total'] : 0);
        $sello = isset($meta['sello']) ? substr($meta['sello'], -8) : '';

        return "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?id={$uuid}&re={$emisor}&rr={$receptor}&tt={$total}&fe={$sello}";
    }

    public function values()
    {
        if (!empty($this->_values)) {
            return $this->_values;
        }

        $version = $this->getVersion();

        if ((string)$this->sxml->attributes()->Version !== $this->getVersion()) {
            throw new UnexpectedValueException("El cfdi no corresponde con la versión }{$version}");
        }

        $meta = [];

        $ns = $this->sxml->getNamespaces(true);

        if (empty($ns['cfdi'])) {
            throw new UnexpectedValueException("Cfdi namespace is not defined");
        }

        if (empty($ns['tfd'])) {
            throw new UnexpectedValueException("Tfd namespace is not defined");
        }

        $this->sxml->registerXPathNamespace('c', $ns['cfdi']);
        $this->sxml->registerXPathNamespace('t', $ns['tfd']);

        $emisor = current($this->sxml->xpath('//c:Comprobante//c:Emisor'));
        $receptor = current($this->sxml->xpath('//c:Comprobante//c:Receptor'));
        $tfd = current($this->sxml->xpath('//t:TimbreFiscalDigital'));

        $meta = [
            'rfc_emisor' => (string)$emisor->attributes()->Rfc,
            'rfc_receptor' => (string)$receptor->attributes()->Rfc,
            'uuid' => (string)$tfd->attributes()->UUID,
            'total' => $this->formatTotal((string)$this->sxml->attributes()->Total),
            'sello' => (string)$this->sxml->attributes()->Sello
        ];

        unset($this->sxml);

        return $this->_values = $meta;
    }

    public function url(array $meta = [])
    {
        return $this->make($meta);
    }

    protected function formatTotal($value)
    {
        $value = number_format(floatval($value), 6, '.', '');

        $total = rtrim($value, '0');

        if (substr($total, -1, 1) === '.') {
            $total = $total . '0';
        }

        return $total;
    }

}
