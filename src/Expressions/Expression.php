<?php
namespace SatSuite\CfdiStatus\Expressions;

use SimpleXMLElement;
use SatSuite\Support\Traits\WithXmlLoader;
use SatSuite\CfdiStatus\Contracts\ExpressionInterface;

abstract class Expression implements ExpressionInterface
{
    use WithXmlLoader;

    /**
     * SimpleXMLElement holder
     *
     * @var SimpleXMLElement
     */
    protected $sxml;

    /**
     * Parsed values
     *
     * @var array
     */
    protected $_values = [];

    abstract public function getVersion();

    /**
     * Load xml from filesystem or string
     *
     * @param string $xml
     *
     * @return Expression
     */
    public function load($xml)
    {
        $this->sxml = $this->getXmlAsSimpleXmlElement($xml);

        $this->_values = [];

        unset($file, $xml);

        return $this;
    }

    /**
     * Set the xml contents
     *
     * @param string $xml
     *
     * @return Expression
     */
    public function with($xml)
    {
        $this->sxml = $this->getXmlAsSimpleXmlElement($xml);

        $this->_values = [];

        return $this;
    }

    /**
     * Process
     *
     * @param array $meta
     *
     * @return string
     */
    abstract public function make(array $meta = []);

    abstract public function values();

    abstract public function url(array $meta = []);


}