<?php
namespace SatSuite\CfdiStatus\Expressions;

use UnexpectedValueException;
use SatSuite\Support\RetencionVersionDiscovery;
use SatSuite\CfdiStatus\Contracts\ExpressionInterface;

class Retenciones
{
    protected $discovery;
    protected $strategies = [];

    public function __construct(RetencionVersionDiscovery $discovery = null)
    {
        $this->discovery = $discovery ?: new RetencionVersionDiscovery();

        $this->defaults();
    }

    /**
     * Register the defaults expression builders
     *
     * @return Retenciones
     */
    public function defaults()
    {
        $this->extend(new Retenciones10());

        return $this;
    }


    /**
     * Loads a given xml file or a xml string
     *
     * @param string $xml
     *
     * @return ExpressionInterface
     */
    public function load($xml)
    {
        $version = $this->discovery->make($xml);

        return $this->using($version)->load($xml);
    }

    /**
     * Extend using new expression builder
     *
     * @param ExpressionInterface $strategy
     *
     * @return Retenciones
     */
    public function extend(ExpressionInterface $strategy)
    {
        $this->strategies[$strategy->getVersion()] = $strategy;
    }

    /**
     * Get a given version strategy for expression builder
     *
     * @param string $version
     *
     * @return ExpressionInterface
     */
    public function using($version)
    {
        $strategy = isset($this->strategies[$version]) ? $this->strategies[$version] : null;

        if (!$strategy) {
            throw new UnexpectedValueException("There is no expression defined for version {$version}");
        }

        return $strategy;
    }
}