<?php
namespace SatSuite\CfdiStatus\Expressions;

use UnexpectedValueException;

class Retenciones10 extends Expression
{
    public function getVersion()
    {
        return '1.0';
    }

    public function make(array $meta = [])
    {
        $meta = $meta ? : $this->values();

        $emisor = htmlspecialchars(trim(isset($meta['rfc_emisor']) ? $meta['rfc_emisor'] : ''), ENT_XML1);
        $receptorNacional = htmlspecialchars(trim(isset($meta['rfc_receptor_nacional']) ? $meta['rfc_receptor_nacional'] : ''), ENT_XML1);
        $receptorExtranjero = htmlspecialchars(trim(isset($meta['rfc_receptor_extranjero']) ? $meta['rfc_receptor_extranjero'] : ''), ENT_XML1);
        $uuid = isset($meta['uuid']) ? $meta['uuid'] : '';
        $total = number_format(isset($meta['total']) ? $meta['total'] : '', 6, '.', '');

        $prefix = $receptorNacional ? 'rr' : 'nr';

        $receptor = $receptorNacional ? : $receptorExtranjero;

        return "?re={$emisor}&{$prefix}={$receptor}&tt={$total}&id={$uuid}";
    }

    public function values()
    {
        if (!empty($this->_values)) {
            return $this->_values;
        }

        if ((string)$this->sxml->attributes()->Version !== '1.0') {
            throw new UnexpectedValueException("Version is not 1.0");
        }

        $meta = [];

        $ns = $this->sxml->getNamespaces(true);

        if (empty($ns['retenciones'])) {
            throw new UnexpectedValueException("Retenciones namespace is not defined");
        }

        if (empty($ns['tfd'])) {
            throw new UnexpectedValueException("Tfd namespace is not defined");
        }

        $this->sxml->registerXPathNamespace('r', $ns['retenciones']);
        $this->sxml->registerXPathNamespace('t', $ns['tfd']);

        $totales = current($this->sxml->xpath('//r:Retenciones//r:Totales'));
        $emisor = current($this->sxml->xpath('//r:Retenciones//r:Emisor'));
        $receptorNacional = current($this->sxml->xpath('//r:Retenciones//r:Receptor//r:Nacional'));
        $receptorExtranjero = current($this->sxml->xpath('//r:Retenciones//r:Receptor//r:Extranjero'));

        $tfd = current($this->sxml->xpath('//t:TimbreFiscalDigital'));

        $meta = [
            'rfc_emisor' => (string)$emisor->attributes()->RFCEmisor,
            'rfc_receptor_nacional' => $receptorNacional ? (string)$receptorNacional->attributes()->RFCRecep : '',
            'rfc_receptor_extranjero' => $receptorExtranjero ? (string)$receptorExtranjero->attributes()->NumRegIdTrib : '',
            'uuid' => (string)$tfd->attributes()->UUID,
            'total' => $this->formatTotal((string)$totales->attributes()->montoTotOperacion),
            'sello' => (string)$this->sxml->attributes()->Sello
        ];

        unset($this->sxml);

        return $this->_values = $meta;
    }

    public function url(array $meta = [])
    {
        $expression = $this->make($meta);

        return "https://prodretencionverificacion.clouda.sat.gob.mx{$expression}";
    }

    protected function formatTotal($value)
    {
        $value = number_format(floatval($value), 6, '.', '');

        $total = rtrim($value, '0');

        if (substr($total, -1, 1) === '.') {
            $total = $total . '0';
        }

        return $total;
    }

}