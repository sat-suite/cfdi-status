<?php
namespace SatSuite\CfdiStatus\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ConnectException;
use SatSuite\CfdiStatus\Contracts\HttpClientInterface;

class GuzzleHttpClient implements HttpClientInterface
{
    public function call($url, $body, array $headers = [])
    {
        $status = 503;
        $contents = null;

        try {
            $client = new Client([
                'verify' => false,
                'timeout'  => 30
            ]);

            $response = $client->post($url, [
                'headers' => $headers,
                'body' => $body
            ]);

            $status = $response->getStatusCode();

            $contents = $response->getBody()->getContents();
        } catch (ClientException $e) {
            $status = $e->getResponse()->getStatusCode();
            $contents = $e->getMessage();
        } catch (ServerException $e) {
            $status = $e->getResponse()->getStatusCode();
            $contents = $e->getMessage();
        } catch (ConnectException $e) {
            $status = 503;
            $contents = $e->getMessage();
        }

        return [
            'status' => $status,
            'body' => $contents
        ];
    }

}