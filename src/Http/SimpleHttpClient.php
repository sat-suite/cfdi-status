<?php
namespace SatSuite\CfdiStatus\Http;

use Throwable;
use SatSuite\CfdiStatus\Contracts\HttpClientInterface;

class SimpleHttpClient implements HttpClientInterface
{
    public function call($url, $body, array $headers = [])
    {
        $_headers = [];

        foreach ($headers as $key => $value) {
            $_headers[] = "{$key}: {$value}";
        }

        $context = stream_context_create([
            'http' => [
                'method'        => 'POST',
                'header'        => implode("\r\n", $_headers),
                'content'       => $body,
                'ignore_errors' => true,
                'timeout' => 30,
            ],
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
            ],
        ]);

        $status = 503;
        $contents = null;

        try {
            $contents = file_get_contents($url, false, $context);

            $status = $this->getHttpCode($http_response_header);
        } catch (Throwable $th) {
            $status = 503;
            $contents = $th->getMessage();
        }

        return [
            'status' => $status,
            'body' => $contents
        ];
    }

    protected function getHttpCode($headers)
    {
        if (is_array($headers)) {
            $parts = explode(' ', $headers[0]);

             //HTTP/1.0 <code> <text>
            if (count($parts) > 1) {
                return intval($parts[1]);
            }
        }

        return 503;
    }
}