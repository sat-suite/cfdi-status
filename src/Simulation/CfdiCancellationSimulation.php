<?php
namespace SatSuite\CfdiStatus\Simulation;

use SatSuite\CfdiStatus\Status\CfdiStatus;
use SatSuite\CfdiStatus\Simulation\SatSimulation;

class CfdiCancellationSimulation extends SatSimulation
{
    public function simulate(array $result, array $meta, $type = null)
    {
        $status = new CfdiStatus($result);

        $cancellable = $this->isCancellableWithNoApproval($meta);

        if ($type !== 'no_cancelable' && $status->isCancellable() && $cancellable) {
            $result = $this->getCancellationWithNoApprovalBody();
        } else if ($type === 'no_cancelable'){
            $result = $this->getNoCancellableBody();
        } else if (!$type) {
            $result = $this->getCancellableWithApprovalBody($type, $meta);
        } else {
            $result = $this->getStatusSimulationBody($type, $meta);
        }

        return $result;
    }
}
