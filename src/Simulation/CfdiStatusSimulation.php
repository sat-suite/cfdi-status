<?php
namespace SatSuite\CfdiStatus\Simulation;

class CfdiStatusSimulation extends SatSimulation
{
    public function simulate(array $meta, $type = null)
    {
        $result = [];

        $cancellable = $this->isCancellableWithNoApproval($meta);

        if ($type !== 'no_cancelable' && $cancellable) {
            $result = $this->getCancellableWithNoApprovalBody();
        } else if ($type === 'no_cancelable'){
            $result = $this->getNoCancellableBody();
        } else if (!$type) {
            $result = $this->getCancellableWithApprovalBody($type, $meta);
        } else {
            $result = $this->getStatusSimulationBody($type, $meta);
        }

        return $result;
    }

}
