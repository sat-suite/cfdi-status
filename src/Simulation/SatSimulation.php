<?php
namespace SatSuite\CfdiStatus\Simulation;

use DateTime;

abstract class SatSimulation
{
    protected $minutes = 15;

    public function threshold($minutes)
    {
        $this->threshold = $minutes;

        return $this;
    }

    public function getThreshold()
    {
        return $this->threshold;
    }

    public function isIssuedDateLessThanThreeDays($createdAt)
    {
        $diff = $this->getDiffInMinutes($createdAt, new DateTime());

        return $diff < 15;
    }

    public function isApprovalTimeExpired($requestDate)
    {
        $diff = $this->getDiffInMinutes($requestDate, new DateTime());

        return $diff > 15;
    }

    public function isCancellableWithNoApproval(array $meta)
    {
        $cancellable = false;

        $date = isset($meta['fecha_comprobante']) ? $meta['fecha_comprobante'] : date('Y-m-d H:i:s');
        $total = isset($meta['total']) ? $meta['total'] : 0;
        $tipo = isset($meta['tipo']) ? $meta['tipo'] : '';
        $rfcReceptor = isset($meta['receptor_rfc']) ? $meta['receptor_rfc'] : '';

        if ($total <= 5000 && in_array($tipo, ['I'])) {
            $cancellable = true;
        } elseif (in_array($tipo, ['N', 'E', 'T'])) {
            $cancellable = true;
        } elseif (in_array($rfcReceptor, ['XAXX010101000', 'XEXX010101000'])) {
            $cancellable = true;
        } else if ($this->isIssuedDateLessThanThreeDays($date)) {
            $cancellable = true;
        }

        return $cancellable;
    }

    protected function getDiffInMinutes($startDate, $nexDate)
    {
        $startDate = new DateTime($startDate);
        $sinceStart = $startDate->diff(new DateTime());

        $diff = $sinceStart->days * 24 * 60;
        $diff += $sinceStart->h * 60;
        $diff += $sinceStart->i;

        return $diff;
    }

    public function getCancellationWithNoApprovalBody()
    {
        return [
            'CodigoEstatus' => 'S - Cancelado sin aceptación.',
            'EsCancelable' => 'Cancelable sin aceptación',
            'Estado' => 'Cancelado',
            'EstatusCancelacion' => 'Cancelado sin aceptación',
        ];
    }

    public function getCancellableWithNoApprovalBody()
    {
        return [
            'CodigoEstatus' => 'S - Comprobante obtenido satisfactoriamente.',
            'EsCancelable' => 'Cancelable sin aceptación',
            'Estado' => 'Vigente',
            'EstatusCancelacion' => '',
        ];
    }

    public function getCancellableWithApprovalBody()
    {
        return [
            'CodigoEstatus' => 'S - Comprobante obtenido satisfactoriamente.',
            'EsCancelable' => 'Cancelable con aceptación',
            'Estado' => 'Vigente',
            'EstatusCancelacion' => '',
        ];
    }

    public function getNoCancellableBody()
    {
        return [
            'CodigoEstatus' => 'S - No cancelable.',
            'EsCancelable' => 'No cancelable',
            'Estado' => 'Vigente',
            'EstatusCancelacion' => 'No Cancelable',
        ];
    }

    public function getStatusSimulationBody($type, array $meta)
    {
        $result = [];
        $date = isset($meta['fecha_cancelacion']) ? $meta['fecha_cancelacion'] : date('Y-m-d H:i:s');

        if ($type === 'no_cancelable') {
            $result = [
                'CodigoEstatus' => 'S - Comprobante obtenido satisfactoriamente.',
                'EsCancelable' => 'No cancelable',
                'Estado' => 'Vigente',
                'EstatusCancelacion' => 'No cancelable',
            ];
        } else if ($type === 'cancelado') {
            $result = [
                'CodigoEstatus' => 'S - Comprobante obtenido satisfactoriamente.',
                'EsCancelable' => 'Cancelable sin aceptación',
                'Estado' => 'Cancelado',
                'EstatusCancelacion' => 'Cancelado sin aceptación',
            ];
        } else if ($type === 'autorizado') {
            $result = [
                'CodigoEstatus' => 'S - Comprobante obtenido satisfactoriamente.',
                'EsCancelable' => 'Cancelable con aceptación',
                'Estado' => 'Cancelado',
                'EstatusCancelacion' => 'Cancelado con aceptación',
            ];
        } elseif ($type === 'rechazado') {
            $result = [
                'CodigoEstatus' => 'S - Solicitud rechazada.',
                'EsCancelable' => 'Cancelable con aceptación',
                'Estado' => 'Vigente',
                'EstatusCancelacion' => 'Solicitud rechazada',
            ];
        } elseif ($this->isApprovalTimeExpired($date)) {
            $result = [
                'CodigoEstatus' => 'S - Comprobante obtenido satisfactoriamente.',
                'EsCancelable' => 'Cancelable con aceptación',
                'Estado' => 'Cancelado',
                'EstatusCancelacion' => 'Plazo vencido',
            ];
        } else {
            $result = [
                'CodigoEstatus' => 'S - En proceso.',
                'EsCancelable' => 'Cancelable con aceptación',
                'Estado' => 'Vigente',
                'EstatusCancelacion' => 'En proceso',
            ];
        }

        return $result;
    }

}
