<?php
namespace SatSuite\CfdiStatus\Status;

use Xaamin\Enum\Enum;

/**
 * @method static self cancellableWithApproval()
 * @method static self isCancellableWithApproval()
 * @method static self cancellableWithNoApproval()
 * @method static self isCancellableWithNoApproval()
 * @method static self undefined()
 * @method static self isUndefined()
 */
class CfdiCancellable extends Enum
{
    protected $enum = [
        'cancellableWithApproval' => 'Cancelable con aceptación',
        'cancellableWithNoApproval' => 'Cancelable sin aceptación',
        'undefined' => 'Undefined'
    ];
}