<?php
namespace SatSuite\CfdiStatus\Status;

use Xaamin\Enum\Enum;

/**
 * @method static self auto()
 * @method static self isAuto()
 * @method static self expiration()
 * @method static self isExpiration()
 * @method static self approved()
 * @method static self isApproved()
 * @method static self processing()
 * @method static self isProcessing()
 * @method static self rejected()
 * @method static self isRejected()
 * @method static self undefined()
 * @method static self isUndefined()
 */
class CfdiCancellationStatus extends Enum
{
    protected $enum = [
        'auto' => 'Cancelado sin aceptación',
        'expiration' => 'Plazo vencido',
        'approved' => 'Cancelado con aceptación',
        'processing' => 'En proceso',
        'rejected' => 'Solicitud rechazada',
        'undefined' => 'Undefined'
    ];
}