<?php
namespace SatSuite\CfdiStatus\Status;

use Xaamin\Enum\Enum;

/**
 * @method static self found()
 * @method static self notFound()
 * @method static self isFound()
 * @method static self isNotFound()
 * @method static self undefined()
 * @method static self isUndefined()
 */
class CfdiQueryStatus extends Enum
{
    protected $enum = [
        'found' => 'S',
        'notFound' => 'N',
        'undefined' => 'Undefined'
    ];
}