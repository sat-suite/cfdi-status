<?php
namespace SatSuite\CfdiStatus\Status;

use Xaamin\Enum\Enum;

/**
 * @method static self valid()
 * @method static self isValid()
 * @method static self cancelled()
 * @method static self isCancelled()
 * @method static self undefined()
 * @method static self isUndefined()
 */
class CfdiSatStatus extends Enum
{
    protected $enum = [
        'valid' => 'Vigente',
        'cancelled' => 'Cancelado',
        'undefined' => 'Undefined'
    ];
}