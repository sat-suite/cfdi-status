<?php
namespace SatSuite\CfdiStatus\Status;

class CfdiStatus
{
    protected $result;

    public $consulta;
    public $estado;
    public $cancelable;
    public $cancelacion;

    private $query;
    private $status;
    private $cancellable;
    private $cancellation;

    public function __construct(array $result)
    {
        $this->result = $result;

        $this->consulta = isset($result['CodigoEstatus']) ? $result['CodigoEstatus'] : '';
        $this->cancelable = isset($result['EsCancelable']) ? $result['EsCancelable'] : '';
        $this->estado = isset($result['Estado']) ? $result['Estado'] : '';
        $this->cancelacion = isset($result['EstatusCancelacion']) ? $result['EstatusCancelacion'] : '';

        $this->estado = is_array($this->estado) ? '' : $this->estado;

        $this->makeEnums();
    }

    public function found()
    {
        return $this->query()->isFound();
    }

    public function query()
    {
        return $this->query;
    }

    public function status()
    {
        return $this->status;
    }

    public function cancellationStatus()
    {
        return $this->cancelacion;
    }

    public function cancellable()
    {
        return $this->cancellable;
    }

    public function cancellation()
    {
        return $this->cancellation;
    }

    public function isValid()
    {
        return $this->query()->isFound() && $this->status()->isValid();
    }

    public function isProcessing()
    {
        return $this->cancellation()->isProcessing();
    }

    public function isCancelled()
    {
        return $this->status()->isCancelled();
    }

    public function isCancellable()
    {
        return in_array($this->cancelable, ['Cancelable sin aceptación', 'Cancelable con aceptación']);
    }

    public function isNotCancellable()
    {
        return !$this->isCancellable();
    }

    public function isCancellableWithApproval()
    {
        return $this->cancellable()->isCancellableWithApproval();
    }

    public function isCancellableWithNoApproval()
    {
        return $this->cancellable()->isCancellableWithNoApproval();
    }

    protected function makeEnums()
    {
        $segments = explode(' - ', $this->consulta);
        $segments = array_map('trim', $segments);

        $this->query = CfdiQueryStatus::search(array_shift($segments)) ? : CfdiQueryStatus::undefined();
        $this->status = CfdiSatStatus::search($this->estado) ? : CfdiSatStatus::undefined();
        $this->cancellable = CfdiCancellable::search($this->cancelable) ? : CfdiCancellable::undefined();
        $this->cancellation = CfdiCancellationStatus::search($this->cancelacion) ? : CfdiCancellationStatus::undefined();
    }

    public function toArray()
    {
        return $this->result;
    }

    protected function toJson()
    {
        return json_encode($this->toArray());
    }

    public function __toString()
    {
        return $this->toJson();
    }
}
